var token = process.env.LINE_TOKEN;
var proxy = process.env.PROXY;

var fs = require('fs');
var http = require("request");

if (proxy) {
    http = http.defaults({ proxy: proxy });
}

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

// var schedule = require('node-schedule');

const days = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัส", "วันศุกร์", "วันเสาร์"];

var today = encodeURIComponent(days[new Date().getDay()]);
var hello = encodeURIComponent("สวัสดี");


// var j = schedule.scheduleJob('0 0 10 * * *', function () {
    http.get({ url: `https://www.google.com/search?q=${hello}+${today}&rlz=1C1CHZL_enTH831TH832&source=lnms&tbm=isch&sa=X&ved=0ahUKEwj_7PDy4sLjAhUFmI8KHX4NAPcQ_AUIESgB&biw=2560&bih=632`, headers: { "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36" } }, (err, httpres, body) => {
        const dom = new JSDOM(body);
        let index = Math.floor(Math.random() * 20) + 1

        var data = JSON.parse(dom.window.document.querySelector("#rg_s > div:nth-child(" + index + ") > div").textContent);

        // let imgUrl = dom.window.document.body.getElementsByTagName("img")[index].getAttribute("src");
        let imgUrl = data.ou

        let filename = "tempfile.jpg";
        http.head(imgUrl, function (err, res, body) {
            console.log('content-type:', res.headers['content-type']);
            console.log('content-length:', res.headers['content-length']);

            http(imgUrl).pipe(fs.createWriteStream(filename)).on('close', () => {
                var req = http.post({
                    url: "https://notify-api.line.me/api/notify", headers: {
                        method: "POST",
                        "Content-Type": "multipart/form-data",
                        Authorization: "Bearer " + token,
                    }
                }, (err, res, body) => {
                })

                var form = req.form();
                form.append("message", `สวัสดี : ${days[new Date().getDay()]}`);
                form.append('imageFile', fs.createReadStream(filename), {
                    filename: 'tempfile.jpg',
                    contentType: 'image/jpeg'
                });
            });
        });
    });
// });
